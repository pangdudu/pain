Gem::Specification.new do |s|
  s.name = %q{pain}
  s.version = "0.1.1"
  s.specification_version = 2 if s.respond_to? :specification_version=
  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["pangdudu"]
  s.date = %q{2009-09-03}
  s.description = %q{Probabilistic Artificial INtelligence, that will never work.}
  s.email = %q{pangdudu@github}
  s.extra_rdoc_files = ["README.rdoc"]
  s.files = ["README.rdoc", "lib/pain.rb",
             "lib/grey.rb","lib/memory.rb"]
  s.has_rdoc = true
  s.homepage = %q{http://github.com/pangdudu/pain}
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.1}
  s.summary = %q{PAIN, a nightmare.}
  s.add_dependency(%q<pangdudu-rofl>, [">= 0"])
  s.add_dependency(%q<pangdudu-rwikibot>, [">= 0"])
  s.add_dependency(%q<sequel>, [">= 0"])
end
