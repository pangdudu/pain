#This class is our long term memory, i.e. an interface to the DB we store the data in
class PrefrontalCortex
  attr_accessor :db #DB connection
  
  def initialize state=:awake
    @db = Sequel.connect(:adapter=>'mysql', :host=>'localhost', :database=>'pain', :user=>'pain', :password=>'pain!secret')
    brainstate state
  end
    
  #get or set the brainstate
  def brainstate state
    if defined? state
      @state = state
      #some state specific stuff
      setup_tables if state.eql? :amnesia
    else
      return @state
    end
  end
  
  #create table structure
  def setup_tables
    @db.tables.each { |table| @db.drop_table table } #drop old tables
    #now we (re-)create the tables related to 'words'
    @db.create_table :words do
      primary_key :id
      String :content, :unique => true, :null => false
      Timestamp :create
      index :content #to speed things up
    end
    @db.create_table :word_transitions do
      Integer :id, :unique => true, :null => false, :auto_increment => true
      Integer :from, :null => false
      Integer :to, :null => false
      Integer :count, :default => 1, :null => false
      Timestamp :update
      primary_key([:from, :to])
    end
    @db.create_table :word_sequences do
      Integer :id, :unique => true, :null => false, :auto_increment => true
      Integer :start, :null => false
      Integer :end, :null => false
      Integer :length, :null => false
      Integer :count, :default => 1
      Integer :varcount, :default => 0
      Float   :score, :default => 0
      Boolean :generalized, :default => false
      String  :sequence, :unique => true, :null => false
      Timestamp :update
      index :start
      index :end
      index :sequence
    end
    #as well as the tables related to 'tags'
    @db.create_table :tags do
      Integer :id, :unique => true, :null => false, :auto_increment => true
      String :value, :null => false
      String :type, :null => false, :default => "simple"
      String :tagger, :null => false, :default => "default"
      Timestamp :create
      primary_key([:value, :type, :tagger])
      #to speed things up
      index :id
      index :value
      index :tagger
      index :type
    end
    @db.create_table :taggings do
      Integer :id, :unique => true, :null => false, :auto_increment => true
      Integer :tag, :null => false
      Integer :target, :null => false
      Integer :count, :default => 1, :null => false
      Timestamp :update
      primary_key([:tag, :target])
    end
  end
end
