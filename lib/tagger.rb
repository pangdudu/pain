=begin
  In this file you find various taggers used to tag supplied input
=end

#Simple default tagger
class DefaultTagger
  def initialize
  end
  #extract tags from content
  def parse content
    #the default tagger simply returns the supplied content as a tag array
    return [{ :simple => content }]
  end
end

#Simple protocol tagger
class ProtocolTagger
  def initialize
  end
  #extract tags from content
  def parse content
    tags = []
    #list of known protocols
    protocols = [:http,:https,:smb,:ssh]
    #search for known protocols
    protocols.each { |p| tags << { :simple => p } if content.include? "#{p.to_s}://" } 
    return tags
  end
end

#Simple url tagger
class UrlTagger
  def initialize
  end
  #extract tags from content
  def parse content
    tags = []
    #list of known protocols associated with urls
    protocols = [:http,:https,:smb,:ssh]
    #search for known protocols
    protocols.each do |p|
      expr = "#{p.to_s}://"
      tags << { :simple => content.gsub(expr,"").strip } if content.include? expr
    end
    return tags
  end
end

#Wikipedia tagger
class WikipediaTagger
  def initialize
    @wikiurl = ".wikipedia.org/wiki/"
  end
  #extract tags from content
  def parse content
    #first of all check if there's some wiki spirit in the content
    if content.include? @wikiurl
      tag = {}
      urlsplit = content.gsub("http://","").split(@wikiurl)
      #language
      tag[:language] = urlsplit[0]
      #article and section
      if urlsplit[1].include? "#"
        tag[:article] = urlsplit[1].split("#")[0]
        tag[:section] = urlsplit[1].split("#")[1]
      else
        tag[:article] = urlsplit[1]
      end
      return [tag]      
    else
      return []
    end
  end
end
