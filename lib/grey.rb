require 'tagger'

#This class tries to implement some language 'processing'
class Broca
  def initialize wernicke
    @wernicke = wernicke
    @tagger = load_tagger #load content tagger
  end

  #process a received hint
  def hint hint,task
    dlog "Thinking about \'#{task}\'."
    dlog "Received a hint \'#{hint}\'."
    tags = update_tags(extract_tags hint)
    #retrieve task associated memories
    memories = retrieve_associated_memories task
    #tag task associated memories with the extracted tags
    tag_memory memories[:associations],tags unless tags.empty?
  end
    
  #tag a memory (and associated memories/sequences) with the supplied tags
  def tag_memory memories,tags
    #tag memory and associated sequences
    memories.each { |id| tag_sequence id,tags } unless memories.nil?
  end
  
  #tag a (by id specified) sequence with (by id identified) tags
  def tag_sequence sequence,tags
    mtaggings = @wernicke.memory.db[:taggings] #taggings in memory
    tags.each do |tag|
      begin
        mtaggings.insert(:tag => tag, :target => sequence)
      rescue Sequel::DatabaseError #tagging is already in memory, increase count
        tgn = mtaggings.filter(:tag => tag, :target => sequence)
        tgn.update(:count => tgn.first[:count]+1)
      end    
    end 
  end
  
  #retrieve entity associated memory ids
  def retrieve_associated_memories entity
    memories = {}
    #get associated sequences from memory
    sequences = @wernicke.generalize_sequence(@wernicke.retrieve_id_sequence(entity.first))
    memories[:sequences] = sequences
    memories[:associations] = retrieve_associated_sequences(sequences)
    return memories
  end
  
  #retrieve the word_sequence ids associated to the sequences
  def retrieve_associated_sequences sequences
    ids = []
    msequences = @wernicke.memory.db[:word_sequences] #word sequences in memory
    sequences.each do |s| 
      first = msequences.filter(:sequence => s.join(" ")).first
      ids << first[:id] unless first.nil?
    end
    return ids
  end
  
  #update tags in long term memory and retrieve ids
  def update_tags tags
    ids = []
    tags.each { |tgr,ttt| ttt.each { |tt| tt.each { |tp,t| ids << update_tag(t,tp,tgr) } } }
    return ids.uniq
  end
  
  #update a single tag and retrieve the id
  def update_tag tag,type,tagger
    tags = @wernicke.memory.db[:tags] #tags in memory
    begin
      return tags.insert(:value => tag.to_s, :type => type.to_s, :tagger => tagger.to_s)
    rescue Sequel::DatabaseError #tag is already in memory, retrieve id
      return tags.filter(:value => tag.to_s, :type => type.to_s, :tagger => tagger.to_s).first[:id]
    end
  end
    
  #parse content and extract tags
  def extract_tags content 
    tags = {}
    #get tags from all available taggers
    @tagger.each { |type,tagger| tags[type] = tagger.parse content }
    return tags
  end

  #load the content tagger
  def load_tagger
    tagger = { :default => DefaultTagger.new }
    #additional tagger
    tagger[:protocol] = ProtocolTagger.new
    tagger[:url] = UrlTagger.new
    tagger[:wikipedia] = WikipediaTagger.new
    return tagger
  end
end #Broca class ends here


#This class tries to implement language 'understanding', it's kind of a 'sensor data => abstract data' mapper
class Wernicke
  attr_reader :memory
  
  def initialize memory
    @memory = memory
  end
  
  #memorize a sequence of words
  def memorize sequence
    #sequence of words => sequence of int ids
    sequence = retrieve_id_sequence sequence
    update_transitions sequence #update the transitions
    update_sequence sequence #update the sequence
  end
    
  #think.
  def think
    generalize_sequences
  end
  
  #generalize and update sequences that still need to be processed
  def generalize_sequences
    msequences = @memory.db[:word_sequences] #word (represented by int ids) sequences in memory
    sequences = msequences.filter(:generalized => false) #only select sequences that haven't been generalized
    sequences.each do |sequence|
      generalized = generalize_sequence sequence[:sequence]
      generalized.each { |s| update_sequence s } #update the sequences in the memory
    end
    msequences.filter(:generalized => false).update(:generalized => true)
  end
  
  #generate associated sequences from an id sequence that might include variables
  #this method has a O(n) of 2^n, the old implementation was around n*n!
  def generalize_sequence sequence
    sequences = []
    sequence.map! { |s| s.to_s } if sequence.is_a? Array
    sequence = sequence.split(" ") if sequence.is_a? String
    #we'll use a counter and a binary representation of the sequence, "*" is 1, a number is 0
    n = sequence.length
    max = 2**n - 1
    (0..max).each do |dec|
      bin = ("%0#{n}d" % dec.to_s(2)).scan(/[01]/) #looks like this: ["0", "1"]
      gen = [] #let's build a new generalized sequence
      state = 0 # -1 means "*" was last, 1 means number was last
      #we use the binary sequence to decide where to replace values with "*"
      (0..n-1).each do |i|
        if bin[i].eql?("1")
          gen[i] = "*" if state >= 0
          state = -1
        else
          gen[i] = sequence[i]
          state = 1
        end
      end
      sequences << (gen - [nil])
    end
    return sequences.uniq #there might be duplicates in here
  end
  
  #insert new transition values or raise transition counts
  def update_transitions sequence
    mtransitions = @memory.db[:word_transitions] #word transitions in memory
    sequence.each_index do |i|
      unless sequence[i+1].nil? 
        from,to = sequence[i],sequence[i+1]
        begin
          mtransitions.insert(:from => from, :to => to)
        rescue Sequel::DatabaseError #transition is already in db, raise count
          count = mtransitions.filter(:from => from, :to => to).first[:count]
          mtransitions.filter(:from => from, :to => to).update(:count => count+1)
        end
      end 
    end
  end
  
  #add or update a sequence
  def update_sequence sequence
    mseqs = @memory.db[:word_sequences]
    joined = sequence.join(" ")
    begin
      first,last = sequence.first,sequence.last
      length,varcount = sequence.length,sequence.count("*")
      if sequence.include? "*"
        mseqs.insert(:sequence =>joined, :start =>first, :end =>last, :length =>length, :generalized =>true, :varcount =>varcount)
      else
        mseqs.insert(:sequence => joined, :start => first, :end => last, :length => length)
      end
    rescue Sequel::DatabaseError #sequence is already in db, raise count
      match = mseqs.filter(:sequence => joined)
      match.update(:count => (match.first[:count])+1)
    end
  end
  
  #retrieve matching integer id sequence to a sequence of words
  def retrieve_id_sequence sequence
    sequence = sequence.scan(/\w+|[\.\!\?\,]/) if sequence.is_a? String
    ids = [] #id sequence
    mwords = @memory.db[:words] #words in memory
    sequence.each do |word|
      begin 
        ids << mwords.insert(:content => word) 
      rescue Sequel::DatabaseError #word is already in db, retrieve id
        ids << mwords.filter(:content => word).first[:id]
      end
    end
    return ids
  end
end #Wernicke class ends here
