#This class represents a sensor
class Sensor
  def initialize &block
    percept &block
  end
  #sensor callback that gets called on a percept
  def percept(*args, &block)
    if block
      @percept = block
    elsif @percept
      @percept.call(*args)
    end
  end
end #class Sensor ends here

#This class implements a filter/router for incoming percepts
class PerceptRouter

  def initialize neural_feedback_loop
    @nfl = neural_feedback_loop
  end
  
  #method that gets called on a new perception
  def percept sensors,sensordata
    #look at the incoming data, and classify sensor input
    types = filter_perception sensordata
    #route sensordata
    types.each do |type|
      #we haz sensorz?
      if sensors.has_key? type
        #let the sensor handle the data
        sensors[type].percept sensordata
        #evil voodoo magic ALERT! 
        #'.send' tries to call a method called nfl.'type', which is handled by @nfl.method_missing, if unimplemented
        @nfl.send type,sensordata
      else #handle unhandled sensordata type
        sensors[type].percept type,sensordata
      end
    end
  end

  #look at the incoming perception data, and classify sensor input
  #(simple now, could implement different filter objects later on)
  def filter_perception sensordata
    types = [] #the types we could identify
    #a very basic filter covering the implementation
    if sensordata.is_a? String
      if sensordata.start_with? "HINT:"
        sensordata.gsub!("HINT:","").lstrip! #cut off the "HINT:" part 
        types << :hint
      else #for now everything that's not a hint is a task
        types << :task
      end 
    else
      types << :unknown
    end
    return types
  end
end #PerceptRouter class ends here

#This class represents our short term memory, kind of what we currently think of
class NeuralFeedbackLoop
  def initialize
    @thoughts = {:last => ["... #{Time.now.to_i} little sheep."]}
  end    
  #ground entity
  def ground
    #whom are we dealing with?
    @thoughts[:interactor] = ENV['USER']
  end
  #dynamic programming hack to access variables through unhandled method calls
  def method_missing(method_name, *args)
    method = method_name.to_sym
    @thoughts[:last] << @thoughts[method] if @thoughts.has_key? method
    @thoughts[method] = args unless args.empty?
    return @thoughts[method] #return what we're thinking of right now
  end 
end #NeuralFeedbackLoop class ends here
