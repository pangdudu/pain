#This class implements reasoning on a probabilistic situation model
class ProbabilisticReasoning

  def initialize neural_feedback_loop,broca
    @nfl,@broca = neural_feedback_loop,broca
  end
  
  #infer current interactor desire
  def infer_desire
    dlog "Trying to infer your desire #{@nfl.interactor}."
    infer_task
  end
  
  #infer the current task
  def infer_task
    assumption = @nfl.task #yeah, lame i know, but making it complexer makes no sense here
    dlog "I assume my task is \'#{assumption}\'."
    memories = @broca.retrieve_associated_memories assumption #get all memories associated with the assumption
    dlog "I found \'#{memories[:associations].length}\' associated memories."
    matches = find_probable_matches assumption,memories #try to find matches with the highest probability
    dlog "I found \'#{matches.length}\' probable matches."
  end
  
  #find memory matches with the highest probability
  def find_probable_matches assumption,memories
    matches = {}
    return matches
  end
end
