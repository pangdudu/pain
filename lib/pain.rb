require 'rubygems'
require 'rofl' #logger/tracer
require 'sequel' #db interface toolkit
#project files
require 'cerebellum' #short term memory and time persistance integration
require 'prefrontal' #long term memory
require 'grey' #grey matter and functional areas
require 'frontal' #probabilistic reasoning
#for profiling
#require 'unprof'

#This is where everything is mixed together
class Cortex
  def initialize
    wakeup
  end

  #called on incoming percept
  def percept sensordata
    @sdr.percept @sensors,sensordata
    think
  end
  
  #infer current interactor desire
  def infer_desire
    @reason.infer_desire
  end

  #one atomic think step
  def think
    #happy hippocampus / sensory data integration
    @wernicke.think
  end

  #wakeup and see where we are
  def wakeup
    #fire up cerebral processing units
    wake_cerebral_cortex
    #start 'real' world interfaces/listeners
    wake_sensors
    #influence brainstate
    brainstate :amnesia #wipe long term memory
    #wakeup and ground entity into current situation
    @nfl.ground
  end

  #initialize and link functional entities we need for processing
  def wake_cerebral_cortex
    @nfl = NeuralFeedbackLoop.new #short term memory
    @sdr = PerceptRouter.new @nfl #filter/router for incoming percepts
    @memory = PrefrontalCortex.new #our long term memory
    @wernicke = Wernicke.new @memory #does the language "understanding" (hearing)
    @broca = Broca.new @wernicke #does the language "processing"
    @reason = ProbabilisticReasoning.new @nfl,@broca #probabilistic reasoning
  end
  
  #initialize sensors
  def wake_sensors
    #wicked aye? that way we're safe if we percept on sensors that aren't implemented
    @sensors = Hash.new(Sensor.new { |t,p| dlog "UNHANDLED #{t.to_s.upcase} SENSOR: #{p.inspect}" })
    #sensor for text input, memorizes text fragments and transitions
    @sensors[:task] = Sensor.new { |p| @wernicke.memorize p.scan(/\w+|[\.\!\?\,]/) }
    #sensor that receives hints from the user (used for tagging/scoring)
    @sensors[:hint] = Sensor.new { |p| @broca.hint p,@nfl.task unless p.nil? }
  end

  #influence the current brain state
  def brainstate state
    case state
      when :amnesia then
        wlog "Initiating amnesia state!" 
        @memory.brainstate :amnesia #bootstrap db (drop and recreate tables)
      else dlog "I feel a little like \'#{state.to_s}\'."
    end
  end

  #what to do if all seems strange and unfamiliar to us  
  def method_missing(method_name, *args)
    ilog "I'm afraid I can not do that #{@nfl.interactor}, sorry."
    dlog "Called undefined method \'#{method_name}\' with args \'#{args * ', '}\'."
  end
end #Cortex class ends here

#TEST SESSION

c = Cortex.new
#simple test training data, tasks and hints
c.percept "What is an apple?"
c.percept "HINT: http://en.wikipedia.org/wiki/Apple#Botanical_information"
c.percept "What is a peach?"
c.percept "HINT: http://en.wikipedia.org/wiki/Peach"
c.percept "What is a banana?"
c.percept "HINT: http://en.wikipedia.org/wiki/Banana#Botany"
c.percept "What is a pineapple?"
c.percept "HINT: http://en.wikipedia.org/wiki/Pineapple#Botany"
c.percept "What is the void?"
c.percept "HINT: http://en.wikipedia.org/wiki/Void"
c.percept "Where is the void now?"
c.percept "HINT: http://en.wikipedia.org/wiki/Void#In_fiction"
c.percept "HINT: http://en.wikipedia.org/wiki/Void#In_other_fields"
c.percept "HINT: http://en.wikipedia.org/wiki/Void#In_science_and_engineering"
c.percept "Where is the banana?"
c.percept "HINT: http://en.wikipedia.org/wiki/Banana#Cultivation"
c.percept "What is a dream good for?"
c.percept "HINT: http://en.wikipedia.org/wiki/Dream#Dreams_for_removing_junk"
#try to infer interactor desire
c.infer_desire
